export declare function writeStartFile(info: string, codeDir: string): "[start.sh] The file already exists and will be used as the startup file." | "The startup file is not found, create a [start.sh] as the startup file.";
export declare const getCommand: (deployType: string, name: string) => {
    command: string;
    commandContent: string;
};
