"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@serverless-devs/core");
var base_1 = __importDefault(require("./base"));
var utils_1 = require("./utils");
var WEB_FRAMEWORK = 'devsapp/web-framework';
var RUNTIME = 'nodejs12';
// @ts-ignore
function report(inputs, command) {
    return __awaiter(this, void 0, void 0, function () {
        var credential;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    inputs.props.runtime = RUNTIME;
                    return [4 /*yield*/, core_1.getCredential(inputs.project.access)];
                case 1:
                    credential = _a.sent();
                    core_1.reportComponent('hapi', {
                        command: command,
                        uid: credential.AccountID,
                    });
                    return [2 /*return*/];
            }
        });
    });
}
var Component = /** @class */ (function (_super) {
    __extends(Component, _super);
    function Component() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Component.prototype.getDeployType = function () {
        return __awaiter(this, void 0, void 0, function () {
            var fcDefault;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, core_1.loadComponent('devsapp/fc-default')];
                    case 1:
                        fcDefault = _a.sent();
                        return [4 /*yield*/, fcDefault.get({ args: "web-framework" })];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 部署应用
     * @param inputs
     */
    Component.prototype.deploy = function (inputs) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            var propsFunction, hasCommand, functionName, deployType, _c, commandContent, command, warn, webFramework;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        propsFunction = inputs.props.function;
                        hasCommand = !!((_a = propsFunction === null || propsFunction === void 0 ? void 0 : propsFunction.customContainerConfig) === null || _a === void 0 ? void 0 : _a.command);
                        if (!!hasCommand) return [3 /*break*/, 2];
                        functionName = propsFunction.name || ((_b = inputs.props.service) === null || _b === void 0 ? void 0 : _b.name);
                        return [4 /*yield*/, this.getDeployType()];
                    case 1:
                        deployType = _d.sent();
                        _c = utils_1.getCommand(deployType, functionName), commandContent = _c.commandContent, command = _c.command;
                        warn = utils_1.writeStartFile(commandContent, propsFunction.code.src);
                        this.logger.warn(warn);
                        propsFunction.customContainerConfig || (propsFunction.customContainerConfig = {});
                        propsFunction.customContainerConfig.command = command;
                        _d.label = 2;
                    case 2: return [4 /*yield*/, report(inputs, 'deploy')];
                    case 3:
                        _d.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 4:
                        webFramework = _d.sent();
                        return [4 /*yield*/, webFramework.deploy(inputs)];
                    case 5: return [2 /*return*/, _d.sent()];
                }
            });
        });
    };
    /**
     * 删除应用
     * @param inputs
     */
    Component.prototype.remove = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, report(inputs, 'remove')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.remove(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 构建应用
     * @param inputs
     */
    Component.prototype.build = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, report(inputs, 'build')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.build(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 打印日志
     * @param inputs
     */
    Component.prototype.logs = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, report(inputs, 'logs')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.logs(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 指标数据查看
     * @param inputs
     */
    Component.prototype.metrics = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, report(inputs, 'metrics')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.metrics(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 指标部署函数信息
     * @param inputs
     */
    Component.prototype.info = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, report(inputs, 'info')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.info(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 查看Nas目录
     * @param inputs
     */
    Component.prototype.ls = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, report(inputs, 'ls')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.ls(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 移除Nas文件/目录
     * @param inputs
     */
    Component.prototype.rm = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, report(inputs, 'rm')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.rm(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 拷贝文件到Nas/更新操作
     * @param inputs
     */
    Component.prototype.cp = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, report(inputs, 'cp')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.cp(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    var _a;
    __decorate([
        core_1.HLogger('HAPI'),
        __metadata("design:type", typeof (_a = typeof core_1.ILogger !== "undefined" && core_1.ILogger) === "function" ? _a : Object)
    ], Component.prototype, "logger", void 0);
    return Component;
}(base_1.default));
exports.default = Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsOENBQXdHO0FBQ3hHLGdEQUFtQztBQUNuQyxpQ0FBcUQ7QUFFckQsSUFBTSxhQUFhLEdBQUcsdUJBQXVCLENBQUM7QUFDOUMsSUFBTSxPQUFPLEdBQUcsVUFBVSxDQUFDO0FBYTNCLGFBQWE7QUFDYixTQUFlLE1BQU0sQ0FBQyxNQUFlLEVBQUUsT0FBZTs7Ozs7O29CQUNwRCxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7b0JBQ1oscUJBQU0sb0JBQWEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFBOztvQkFBdkQsVUFBVSxHQUFHLFNBQTBDO29CQUU3RCxzQkFBZSxDQUFDLE1BQU0sRUFBRTt3QkFDdEIsT0FBTyxTQUFBO3dCQUNQLEdBQUcsRUFBRSxVQUFVLENBQUMsU0FBUztxQkFDMUIsQ0FBQyxDQUFDOzs7OztDQUNKO0FBQ0Q7SUFBdUMsNkJBQWE7SUFBcEQ7O0lBcUdBLENBQUM7SUFuR08saUNBQWEsR0FBbkI7Ozs7OzRCQUNvQixxQkFBTSxvQkFBYSxDQUFDLG9CQUFvQixDQUFDLEVBQUE7O3dCQUFyRCxTQUFTLEdBQUcsU0FBeUM7d0JBQ3BELHFCQUFNLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLENBQUMsRUFBQTs0QkFBckQsc0JBQU8sU0FBOEMsRUFBQzs7OztLQUN2RDtJQUNEOzs7T0FHRztJQUNVLDBCQUFNLEdBQW5CLFVBQW9CLE1BQWU7Ozs7Ozs7d0JBQzNCLGFBQWEsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQzt3QkFDdEMsVUFBVSxHQUFHLENBQUMsUUFBQyxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUscUJBQXFCLDBDQUFFLE9BQU8sQ0FBQSxDQUFDOzZCQUMvRCxDQUFDLFVBQVUsRUFBWCx3QkFBVzt3QkFDUCxZQUFZLEdBQUcsYUFBYSxDQUFDLElBQUksV0FBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sMENBQUUsSUFBSSxDQUFBLENBQUM7d0JBRW5ELHFCQUFNLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBQTs7d0JBQXZDLFVBQVUsR0FBRyxTQUEwQjt3QkFDdkMsS0FBOEIsa0JBQVUsQ0FBQyxVQUFVLEVBQUUsWUFBWSxDQUFDLEVBQWhFLGNBQWMsb0JBQUEsRUFBRSxPQUFPLGFBQUEsQ0FBMEM7d0JBQ25FLElBQUksR0FBRyxzQkFBYyxDQUFDLGNBQWMsRUFBRSxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUNwRSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFFdkIsYUFBYSxDQUFDLHFCQUFxQixJQUFJLENBQUMsYUFBYSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNsRixhQUFhLENBQUMscUJBQXFCLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQzs7NEJBR3hELHFCQUFNLE1BQU0sQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLEVBQUE7O3dCQUE5QixTQUE4QixDQUFDO3dCQUNWLHFCQUFNLG9CQUFhLENBQUMsYUFBYSxDQUFDLEVBQUE7O3dCQUFqRCxZQUFZLEdBQUcsU0FBa0M7d0JBQ2hELHFCQUFNLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUE7NEJBQXhDLHNCQUFPLFNBQWlDLEVBQUM7Ozs7S0FDMUM7SUFDRDs7O09BR0c7SUFDVSwwQkFBTSxHQUFuQixVQUFvQixNQUFlOzs7Ozs0QkFDakMscUJBQU0sTUFBTSxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsRUFBQTs7d0JBQTlCLFNBQThCLENBQUM7d0JBQ1YscUJBQU0sb0JBQWEsQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQWpELFlBQVksR0FBRyxTQUFrQzt3QkFDaEQscUJBQU0sWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBQTs0QkFBeEMsc0JBQU8sU0FBaUMsRUFBQzs7OztLQUMxQztJQUNEOzs7T0FHRztJQUNVLHlCQUFLLEdBQWxCLFVBQW1CLE1BQWU7Ozs7OzRCQUNoQyxxQkFBTSxNQUFNLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxFQUFBOzt3QkFBN0IsU0FBNkIsQ0FBQzt3QkFDVCxxQkFBTSxvQkFBYSxDQUFDLGFBQWEsQ0FBQyxFQUFBOzt3QkFBakQsWUFBWSxHQUFHLFNBQWtDO3dCQUNoRCxxQkFBTSxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFBOzRCQUF2QyxzQkFBTyxTQUFnQyxFQUFDOzs7O0tBQ3pDO0lBQ0Q7OztPQUdHO0lBQ1Usd0JBQUksR0FBakIsVUFBa0IsTUFBZTs7Ozs7NEJBQy9CLHFCQUFNLE1BQU0sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLEVBQUE7O3dCQUE1QixTQUE0QixDQUFDO3dCQUNSLHFCQUFNLG9CQUFhLENBQUMsYUFBYSxDQUFDLEVBQUE7O3dCQUFqRCxZQUFZLEdBQUcsU0FBa0M7d0JBQ2hELHFCQUFNLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUE7NEJBQXRDLHNCQUFPLFNBQStCLEVBQUM7Ozs7S0FDeEM7SUFDRDs7O09BR0c7SUFDVSwyQkFBTyxHQUFwQixVQUFxQixNQUFlOzs7Ozs0QkFDbEMscUJBQU0sTUFBTSxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsRUFBQTs7d0JBQS9CLFNBQStCLENBQUM7d0JBQ1gscUJBQU0sb0JBQWEsQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQWpELFlBQVksR0FBRyxTQUFrQzt3QkFDaEQscUJBQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBQTs0QkFBekMsc0JBQU8sU0FBa0MsRUFBQzs7OztLQUMzQztJQUNEOzs7T0FHRztJQUNHLHdCQUFJLEdBQVYsVUFBVyxNQUFlOzs7Ozs0QkFDeEIscUJBQU0sTUFBTSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsRUFBQTs7d0JBQTVCLFNBQTRCLENBQUM7d0JBQ1IscUJBQU0sb0JBQWEsQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQWpELFlBQVksR0FBRyxTQUFrQzt3QkFDaEQscUJBQU0sWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQTs0QkFBdEMsc0JBQU8sU0FBK0IsRUFBQzs7OztLQUN4QztJQUNEOzs7T0FHRztJQUNVLHNCQUFFLEdBQWYsVUFBZ0IsTUFBZTs7Ozs7NEJBQzdCLHFCQUFNLE1BQU0sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUE7O3dCQUExQixTQUEwQixDQUFDO3dCQUNOLHFCQUFNLG9CQUFhLENBQUMsYUFBYSxDQUFDLEVBQUE7O3dCQUFqRCxZQUFZLEdBQUcsU0FBa0M7d0JBQ2hELHFCQUFNLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQUE7NEJBQXBDLHNCQUFPLFNBQTZCLEVBQUM7Ozs7S0FDdEM7SUFDRDs7O09BR0c7SUFDVSxzQkFBRSxHQUFmLFVBQWdCLE1BQWU7Ozs7OzRCQUM3QixxQkFBTSxNQUFNLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFBOzt3QkFBMUIsU0FBMEIsQ0FBQzt3QkFDTixxQkFBTSxvQkFBYSxDQUFDLGFBQWEsQ0FBQyxFQUFBOzt3QkFBakQsWUFBWSxHQUFHLFNBQWtDO3dCQUNoRCxxQkFBTSxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzRCQUFwQyxzQkFBTyxTQUE2QixFQUFDOzs7O0tBQ3RDO0lBQ0Q7OztPQUdHO0lBQ1Usc0JBQUUsR0FBZixVQUFnQixNQUFlOzs7Ozs0QkFDN0IscUJBQU0sTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBQTs7d0JBQTFCLFNBQTBCLENBQUM7d0JBQ04scUJBQU0sb0JBQWEsQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQWpELFlBQVksR0FBRyxTQUFrQzt3QkFDaEQscUJBQU0sWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsRUFBQTs0QkFBcEMsc0JBQU8sU0FBNkIsRUFBQzs7OztLQUN0Qzs7SUFuR2dCO1FBQWhCLGNBQU8sQ0FBQyxNQUFNLENBQUM7c0RBQVMsY0FBTyxvQkFBUCxjQUFPOzZDQUFDO0lBb0duQyxnQkFBQztDQUFBLEFBckdELENBQXVDLGNBQWEsR0FxR25EO2tCQXJHb0IsU0FBUyJ9